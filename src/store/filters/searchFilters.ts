import { ITodo, Status } from 'models';

export const filterByName = (todo: ITodo, searchValue: string): boolean =>
	searchValue ? todo.title.toLowerCase().includes(searchValue) : true;

export const filterByStatus = (todo: ITodo, searchValue: Status): boolean =>
	searchValue !== Status.All ? todo.status === searchValue : true;
