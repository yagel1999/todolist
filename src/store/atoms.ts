import { atom } from 'recoil';
import { Status, ITodo } from 'models';

export const todoList = atom<ITodo[]>({
	key: 'todoList',
	default: []
});

export const textSearch = atom<string>({
	key: 'text search',
	default: ''
});

export const statusSearch = atom<Status>({
	key: 'status search',
	default: Status.All
});
