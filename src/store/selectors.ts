import { selector } from 'recoil';
import { ITodo } from 'models';
import { filterByName, filterByStatus } from './filters/searchFilters';
import { todoList, textSearch, statusSearch } from './atoms';

export const filteredTodoList = selector<ITodo[]>({
	key: 'filtered todolist',
	get: ({ get }) =>
		get(todoList).filter(
			(todo: ITodo) =>
				filterByName(todo, get(textSearch).toLowerCase()) &&
				filterByStatus(todo, get(statusSearch))
		)
});

export const todoListLength = selector<number>({
	key: 'current todolist length',
	get: ({ get }) => get(filteredTodoList).length
});
