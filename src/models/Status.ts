enum Status {
	All = -1,
	Active,
	Completed
}

export default Status;
