import RestApiHandler from './handlers/rest';
import LocalStorageHandler from './handlers/localStorage';
import { jsonPlaceholderMapper } from './mappers/jsonPlaceholder';
import { GraphQLHandler } from './handlers/graphql';

export default (method: typeof process.env.API_METHOD) => {
	switch (method) {
		case 'graphql':
			return new GraphQLHandler('http://localhost:4000/graphql');
		case 'jsonplaceholder':
			return new RestApiHandler(
				'https://jsonplaceholder.typicode.com/todos',
				jsonPlaceholderMapper
			);
		case 'custom_rest':
			return new RestApiHandler('https://localhost:4001');
		case 'local_storage':
		default:
			return new LocalStorageHandler();
	}
};
