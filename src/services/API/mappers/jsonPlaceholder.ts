import { IOldTodo, Status, Mapper } from 'models';

export const jsonPlaceholderMapper: Mapper<IOldTodo> = (oldTodo) => ({
	id: oldTodo.id.toString(),
	title: oldTodo.title,
	status: oldTodo.completed ? Status.Completed : Status.Active
});
