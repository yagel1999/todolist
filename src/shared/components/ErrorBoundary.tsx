import { Component, FC } from 'react';

interface Props {
	fallback: FC;
}

interface State {
	error: Error | null;
}

export default class ErrorBoundry extends Component<Props, State> {
	constructor(props: Props) {
		super(props);

		this.state = {
			error: null
		};
	}

	static getDerivedStateFromError(error: Error): State {
		return {
			error
		};
	}

	render() {
		return this.state.error ? this.props.fallback : this.props.children;
	}
}
