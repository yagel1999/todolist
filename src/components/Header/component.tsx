import React from 'react';
import { useRecoilValue } from 'recoil';
import { todoListLength } from 'store';
import './header.scss';

const Header: React.FC = () => {
	const todolistCount: number = useRecoilValue(todoListLength);

	return (
		<div className='header-container'>
			<span className='title'>The GREATEST TodoList of All-Time</span>
			<span className='subtitle'>Count: {todolistCount}</span>
		</div>
	);
};

export default Header;
