import React from 'react';
import debounce from 'lodash.debounce';
import { Status } from 'models';
import { useUpdateStatusSearch, useUpdateTextSearch } from 'store';
import SearchBarComponent from './component';

const SearchBarContainer: React.FC = () => {
	const updateStatusSearch = useUpdateStatusSearch();
	const updateTextSearch = useUpdateTextSearch();

	const changeText = debounce((text: string) => {
		updateTextSearch(text);
	}, 300);

	const changeStatusHandler = (
		event: React.ChangeEvent<HTMLSelectElement>
	): void => {
		updateStatusSearch(Number(event.target.value) as Status);
	};

	const changeTextHandler = (
		event: React.ChangeEvent<HTMLInputElement>
	): void => {
		changeText(event.target.value);
	};

	return (
		<SearchBarComponent
			textHandler={changeTextHandler}
			listHandler={changeStatusHandler}
		/>
	);
};

export default SearchBarContainer;
