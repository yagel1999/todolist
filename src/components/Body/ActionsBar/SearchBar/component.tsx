/* eslint-disable no-restricted-globals */
import React from 'react';
import { Status } from 'models';
import './search-bar.scss';

interface Props {
	textHandler: (event: React.ChangeEvent<HTMLInputElement>) => void;
	listHandler: (event: React.ChangeEvent<HTMLSelectElement>) => void;
}

type EnumType = [string, string | Status];

const ddlValues: EnumType[] = Object.entries(Status).filter(([key]: EnumType) =>
	isNaN(Number(key))
);

const SearchBarComponent: React.FC<Props> = ({ textHandler, listHandler }) => {
	return (
		<div className='search-bar'>
			<input
				type='text'
				placeholder='Search a Todo'
				className='input-bar'
				onChange={textHandler}
			/>
			<select onChange={listHandler} className='drop-down-list'>
				{ddlValues.map(([key, value]: EnumType) => (
					<option key={key} value={value}>
						{key}
					</option>
				))}
			</select>
		</div>
	);
};

export default SearchBarComponent;
