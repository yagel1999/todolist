import React from 'react';
import './EditName.scss';

interface Props {
	submit: (todoName: string) => void;
	closeEditMode: () => void;
	placeholder: string;
}

const EditNameMode: React.FC<Props> = ({
	submit,
	closeEditMode,
	placeholder
}: Props) => {
	const submitHandler = (value: string): void => {
		if (value.trim()) {
			submit(value);
		}

		closeEditMode();
	};

	const enterHandler = (event: React.KeyboardEvent<HTMLInputElement>): void => {
		if (event.key === 'Enter') {
			submitHandler(event.currentTarget.value);
		}
	};

	const blurHandler = (event: React.FocusEvent<HTMLInputElement>): void => {
		submitHandler(event.currentTarget.value);
	};

	return (
		<input
			type='text'
			className='edit-name-input'
			ref={(ref: HTMLInputElement | null) => ref && ref.focus()}
			placeholder={placeholder}
			onBlur={blurHandler}
			onKeyDown={enterHandler}
		/>
	);
};

export default EditNameMode;
