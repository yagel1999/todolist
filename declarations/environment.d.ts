declare namespace NodeJS {
	export interface ProcessEnv {
		NODE_ENV: 'development' | 'production';
		API_METHOD:
			| 'jsonplaceholder'
			| 'local_storage'
			| 'custom_rest'
			| 'custom_graphql'
			| 'graphql';
	}
}
