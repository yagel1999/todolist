FROM node:16.14.2-alpine as builder

USER root
WORKDIR /tmp

COPY package*.json .
RUN npm ci

COPY . .
RUN npm run build

FROM nginx:1.24.0-alpine-slim

USER root
WORKDIR /usr/share/nginx/html

COPY docker-entrypoint.sh .
COPY --from=builder /tmp/dist .

EXPOSE 80

CMD ["sh", "docker-entrypoint.sh"]